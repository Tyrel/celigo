# testing imports
import unittest
import tempfile
import os
import requests_mock

# package imports
import celigo


class CeligoTest(unittest.TestCase):

    def setUp(self):
        self.FAKE_API_KEY = 'fake'
        os.environ['CELIGO_API_KEY'] = self.FAKE_API_KEY

    def test_needs_all_params(self):
        """
        Test that we need to pass in a data directory.
        """
        with self.assertRaises(TypeError):
            celigo.BackupCeligo()
        data_dir = "fakedir"
        celigo.BackupCeligo(data_dir)

    def test_needs_api_key(self):
        """
        Test that we need to either pass in the api_key,
        or set the CELIGO_API_KEY environment variable.
        """
        data_dir = "fakedir"
        os.environ['CELIGO_API_KEY'] = ''
        with self.assertRaisesRegexp(Exception, 'Please pass in api_key.*'):
            celigo.BackupCeligo(data_dir)
        os.environ['CELIGO_API_KEY'] = self.FAKE_API_KEY
        bc = celigo.BackupCeligo(data_dir)
        # Make sure it uses the env var one
        self.assertEqual(bc.api_key, self.FAKE_API_KEY)

        # Unset env var and make sure it uses the passed in one
        os.environ['CELIGO_API_KEY'] = ''
        bc = celigo.BackupCeligo(data_dir, api_key='fake_passed_in')
        self.assertEqual(bc.api_key, 'fake_passed_in')

    def test_ensure_directories(self):
        """
            Test that the ensure_directories_exist works properly.
        """
        fake_id = "fake_integration_id"
        tempdir = tempfile.mkdtemp('celigo_testing')
        bc = celigo.BackupCeligo(tempdir)

        imports_dir = os.path.join(tempdir, "integrations", fake_id, "imports")
        connections_dir = os.path.join(tempdir, "connections")
        # Check that the directories don't exist already.
        self.assertFalse(
            os.path.exists(imports_dir),
            "imports dir exists")
        self.assertFalse(
            os.path.exists(connections_dir),
            "connections dir exists")

        # Make the directories.
        bc.ensure_directories_exist((imports_dir, connections_dir))
        self.assertTrue(
            os.path.exists(imports_dir),
            "Did not create proper directory"
            )
        self.assertTrue(
            os.path.exists(connections_dir),
            "Did not create proper directory"
            )

        # Make sure nothing errors if the directories exist already.
        bc.ensure_directories_exist((imports_dir, connections_dir))

    # @requests_mock.Mocker()
    # def test_fake_requests(self, rqm):
    #     rqm.get(celigo.DEFAULT_BASE_URL, text='resp')
    #     tempdir = tempfile.mkdtemp('celigo_testing')
    #     bc = celigo.BackupCeligo(tempdir)
