Celigo Backup
-------------

This module interacts with the Celigo API.

If you have any Celigo flows that have a NetSuite connection, and you wish
to backup the field mappings for the flow, then use this.
