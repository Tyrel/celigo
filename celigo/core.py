import glob
import collections
import logging
import requests
import io
import os
import json

from slugify import slugify

from .prompt import prompt

logging.basicConfig(level=logging.INFO)
L = logging.getLogger(__name__)
DEFAULT_BASE_URL = "https://api.integrator.io/v1/"


class BackupCeligo(object):
    """
        This module interacts with the Celigo API.
    """
    def __init__(self,
                 data_dir,
                 api_key=None,
                 headers=None,
                 base_url=None,
                 ):
        """
        :param data_dir: The directory to read and store JSON files
            for imports and configurations.
        :param headers: Single level dict of headers to add the the
            api call session.
        :param base_url: Url for the base of the api to call to.
            Default is None, which will just fill in celigo.
        """
        if not base_url:
            base_url = DEFAULT_BASE_URL

        # Set api key if it was passed in
        self.api_key = api_key
        if not self.api_key:
            # if this is still falsy, set from environment variable.
            self.api_key = os.environ.get("CELIGO_API_KEY")
            if not self.api_key:
                # Needs an API key, raise exceptions
                raise Exception(
                    "Please pass in api_key, or set the CELIGO_API_KEY "
                    "environment variable.")
        self.data_dir = data_dir
        self.base_url = base_url
        self.imports_cache = {}
        self.setup_headers(headers)
        self.setup_requests_session()

    def setup_headers(self, headers=None):
        """
            Setup self.headers so that it has, at a minimum
            Authorization, and Content-Type
            Allow any others to be updated.
            :param headers: Single Level dict of headers
        """
        _headers = {}
        _headers['Authorization'] = "Bearer {API_KEY}".format(
            API_KEY=self.api_key)
        _headers['Content-Type'] = "application/json"
        # Set any additional headers
        if headers:
            _headers.update(**headers)
        self.headers = _headers

    def setup_requests_session(self):
        """
            Setup a session using requests.
            This has the benefit of always using the Authorization header,
            and the Content-Type header, rather than configuring it on every
            request.
        """
        self.session = requests.Session()
        self.session.headers.update(self.headers)

    def ensure_directories_exist(self, subdirs=None):
        """ Make the directory if it doesn't exist """
        for subdir in subdirs:
            L.info("Creating subdir: `%s`", subdir)
            _dir = os.path.join(self.data_dir, subdir)
            if not os.path.exists(_dir):
                os.makedirs(_dir)

    def _celigo_api_get(self, path):
        """
            Make a GET request to the celigo API

            :param path: The rest of the path after BASE_URL
            :return conf_dict: response as a dict of the json returned from the
                api.
        """
        response = self.session.get(url=self.base_url + path)
        response.raise_for_status()
        conf_dict = response.json()
        L.info("Got conf_dict from %s", self.base_url + path)
        return conf_dict

    def _celigo_api_put(self, path, body):
        """
            Make a GET request to the celigo API

            :param path: The rest of the path after BASE_URL
            :return conf_dict: response as a dict of the json returned from the
                api.
        """
        response = self.session.put(
            url=self.base_url + path,
            data=json.dumps(body)
        )
        response.raise_for_status()

        L.info("Restored backup to %s", self.base_url + path)
        return response

    def _get_integration_placeholders(self):
        try:
            integrations = self._celigo_api_get("integrations/")
        except requests.exceptions.RequestException:
            L.info('HTTP Request failed')
            raise
        # Setup integrations dictionaries
        for integration in integrations:
            self.imports_cache[integration['_id']] = {
                    'name': integration['name'],
                    'slug': slugify(integration['name']),
                    'flows': []}

    def _get_flow_configurations(self):
        try:
            flows = self._celigo_api_get("flows/")
            for flow in flows:
                self.cache_import_remote(flow)
        except requests.exceptions.RequestException:
            L.info('HTTP Request failed')
            raise
        L.info("Got all imports, writing now")

    def _save_each_flow(self, auto=False):
        for integration_id, integration in self.imports_cache.items():
            for flow in integration['flows']:
                self.save_flow(integration_id, flow, auto)

    def backup(self, auto=False):
        """
            Get all the flow data from Celigo.
            Then loop over each flow and cache it's Import data in an instance
            varable.
            Once this is cached, save the imports.
        """
        self._get_integration_placeholders()
        self._get_flow_configurations()
        self._save_each_flow(auto)


    def restore(self, auto=False):
        """
            Get all the import files in the import direcotry,
            and store them in the local self.imports_cache cache.
            Once they are stored, prompt on which to restore to Celigo.
        """
        import_dir = os.path.join(self.data_dir, "imports")

        for fname in glob.glob("{}/*_*.json".format(import_dir)):
            self.cache_import_json(fname)

        while self.imports_cache.keys():
            if auto:
                action = "All"
            else:
                options = self.imports_cache.keys()
                options = sorted(options)
                options.append("All")
                options.append("None")
                action = prompt("Backup which import(s)?", options)

            if action == "None":
                # We're done here.
                return
            elif action == "All":
                # Process them all.
                for key in self.imports_cache.keys():
                    self.restore_to_celigo(key)
                break
            else:
                # Process one and remove it from the cache so we don't reupload
                self.restore_to_celigo(action)
                del self.imports_cache[action]

    def cache_import_remote(self, flow):
        """
            Stores the import in self.imports_cache before write.
        """
        flow_name = flow['name']
        import_id = flow['_importId']
        integration_id = flow['_integrationId']
        import_conf = self._celigo_api_get(
            "imports/{id}/distributed".format(
                id=import_id))

        self.imports_cache[integration_id]['flows'].append({
            "name": flow_name,
            "id": import_id,
            "configuration": import_conf
        })

    def save_flow(self, integration_id, flow, auto=False):
        """
            Write the import to a .json file  with name_id.json format.
            Prompt for overwrite.
            :param flow: dictionary of "name", "id", "configuration" for the
                flow.
            :param auto: if auto is true, don't prompt for overwrite
        """
        flow_name = flow['name']
        flow_id = flow['id']
        flow_conf = flow['configuration']

        filedir = os.path.join(self.data_dir,
                               "integrations",
                               integration_id,
                               "imports")

        self.ensure_directories_exist((filedir,))

        filename = os.path.join(
            filedir,
            "%s_%s.json" % (slugify(flow_name), flow_id))
        write = True

        # By default, we prompt for overwrites
        if os.path.isfile(filename) and not auto:

            # Check that we should overwrite
            overwrite = prompt(
                "File {filename} exists, overwrite file?".format(
                    filename=filename)
            )
            write = bool(overwrite == "Yes")

        if write:
            self.write_json(filename, flow_conf)
        else:
            L.info("You chose not to save this file.")

    def write_json(self, filename, contents):
        """
            Write a file of json data and say that we wrote it.
        """
        with io.open(filename, "w", encoding='utf-8') as f:
            f.write(unicode(json.dumps(contents, f, indent=4,
                                       ensure_ascii=False, encoding='utf-8')))
        L.info("Wrote {}".format(filename))

    def cache_import_json(self, fname):
        """
            Load the import from a .json file  with name_id.json format.
        """
        with io.open(fname, "r", encoding='utf-8') as f:
            json_data = json.load(f)
        fname = os.path.split(fname)[1][:-5]
        flow_name, import_id = fname.split("_")
        # Store locally in the instance cache
        self.imports_cache[flow_name] = (import_id, json_data)

    def restore_to_celigo(self, key):
        """
            Make the call to celigo to update the data
            :params key: slugified name to use as a key
        """
        raise NotImplemented("NOT IMPLEMENTED YET")
        # yes this works but I don't want to allow this in testing.
        # TODO: Uncomment.
        # import_id, import_conf = self.imports_cache[key]
        # response = self._celigo_api_put(
        #     "imports/{id}/distributed".format(
        #         id=import_id),
        #     import_conf)
        # L.info("Restored: ")
        # L.info(response.json()['_id'])
        # L.info("")
