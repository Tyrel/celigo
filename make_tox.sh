#!/usr/bin/bash
export PYENV_ROOT="$HOME/.pyenv"    
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
pyenv shell 2.7.11 3.3.6 3.4.4 3.5.1 pypy-5.0.0
pyenv rehash
tox
